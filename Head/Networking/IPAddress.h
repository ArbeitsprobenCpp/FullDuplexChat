#ifndef NETWORKING_IPADDRESS_H
#define NETWORKING_IPADDRESS_H

#include <Base.h>

namespace Networking
{
	/*
	Enumeration fuer IP-Protokolle.
	Aktuell ist nur IPv4 implementiert.
	*/
	struct AddressTypes
	{
	public:
		enum Value
		{
			IPv4
		};

	private:
		Value value;

	public:
		AddressTypes()
			:value()
		{
		}
		AddressTypes(Value v)
			:value(v)
		{
		}

	public:
		Value operator*() const
		{
			return this->value;
		}
		operator Value() const
		{
			return this->value;
		}
	};

	class IPAddress
	{
	private:
		AddressTypes::Value type;

	public:
		explicit IPAddress(AddressTypes::Value type);
		virtual ~IPAddress() {}

	public:
		virtual bool operator==(IPAddress const&) const = 0;
		virtual bool operator!=(IPAddress const&) const = 0;

	public:
		AddressTypes::Value getType() const;

		virtual IPAddress* copy() const = 0;
		virtual std::string toString() const = 0;

	public:
		static IPAddress const& Any(AddressTypes::Value type = AddressTypes::IPv4);
		static IPAddress const& Localhost(AddressTypes::Value type = AddressTypes::IPv4);
	};

	class IPv4Address : public IPAddress
	{
	private:
		uint8_t address[4];

	public:
		explicit IPv4Address(std::string const& address);
		explicit IPv4Address(uint8_t a, uint8_t b, uint8_t c, uint8_t d);

	public:
		bool operator==(IPAddress const&) const;
		bool operator!=(IPAddress const&) const;

	public:
		uint32_t asBigEndian() const;
		uint8_t const* asBytes() const;

		IPv4Address* copy() const;
		std::string toString() const;

	public:
		static IPv4Address FromBigEndian(uint32_t address);
	};
}

#endif