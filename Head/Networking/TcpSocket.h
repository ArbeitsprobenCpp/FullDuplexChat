#ifndef NETWORKING_TCPSOCKET_H
#define NETWORKING_TCPSOCKET_H

#include <Networking/OSSpecific/SocketDefinitions.h>
#include <Networking/Endpoint.h>

#include <Threading/Mutex.h>


namespace Networking
{
	class TcpSocket
	{
	private:
		Threading::Mutex lock;
		int status;

		SocketHandle handle;
		bool ownsHandle;

		Endpoint localEndpoint;
		Endpoint remoteEndpoint;

	private:
		TcpSocket(TcpSocket const&);

	public:
		explicit TcpSocket();
		~TcpSocket();

	private:
		TcpSocket& operator=(TcpSocket const&);

	private:
		void resetHandle(bool);

		//Private Version der Close-Methode, die in synchronisiertem Kontext genutzt werden muss.
		void privateClose();

		void assertConnected() const;

	public:
		/*
		Setzt den Zustand dieser Socket auf Closed und leert sowohl das SocketHandle als auch die Endpunkte.
		Wirft keine Ausnahmen, es sei denn eine Funkion des Betriebssystems verursacht einen Fehler.
		Schliesst ein vorhandenes SocketHandle nur, wenn diese Instanz das Handle besitzt.
		*/
		void close();


		void connect(Endpoint const& ep);
		void connect(IPAddress const& ip, unsigned short port);
		/*
		Erlaubt das Nutzen eines bestehenden SocketHandles mit der TcpSocket Klasse.
		Baut mit dem gegebenen SocketHandle eine Verbindung zum gegebenen Endpunkt auf.
		*/
		void connectWithHandle(Endpoint const&, SocketHandle);

		/*
		Trennt eine Verbindung falls vorhanden. Wirft eine Ausnahme, falls die Socket nicht im Zustand Connected ist.
		Das SocketHandle wird geschlossen, egal ob diese Instanz das Handle besitzt oder nicht.
		*/
		void disconnect();


		void bind(Endpoint const& ep);
		void bind(unsigned short port = Endpoint::AnyPort, IPAddress const& ip = IPAddress::Any());
		/*
		Erlaubt das Nutzen eines bestehenden SocketHandles mit der TcpSocket Klasse.
		Bindet das gegebene SocketHandle an den gegebenen lokalen Endpunkt.
		*/
		void bindWithHandle(Endpoint const&, SocketHandle handle);

		void listen(int backlog = 50);
		
		/*
		Nimmt eine eingehende Verbindung an, wenn die Socket im Zustand Listening ist.
		Diese neue Verbindung wird im gegebenen Buffer-Objekt gespeichert.
		Wirft eine Ausnahme, wenn das buffer-Objekt nicht im Zustand Closed ist.
		*/
		void accept(TcpSocket& buffer);
		/*
		Nimmt eine eingehende Verbindung an, wenn die Socket im Zustand Listening ist.
		Erstellt dazu eine neue, dynamisch allozierte TcpSocket-Instanz.
		*/
		TcpSocket* accept();

		/*
		Empfaengt bis zu bufferSize Bytes von der Verbindung und speichert sie in buffer.
		Gibt die Anzahl empfangener Bytes zurueck.
		*/
		uint32_t receive(char* buffer, uint32_t bufferSize);
		/*
		Sendet bufferSize Bytes im gegebenen buffer ueber die Verbindung.
		*/
		void send(char const* buffer, uint32_t bufferSize);


		bool isClosed() const;
		bool isConnected() const;
		/*
		Falls die Instanz im Zustand Connected ist und der Parameter refreshState true ist wird vor der Rueckgabe die Methode refreshState aufgerufen.
		So wird ermoeglicht, einen Verbindungsabbruch festzustellen, der dieser Instanz bisher noch nicht bekannt war.
		Diese Methode ist nicht ausreichend um bspw. einen Aufruf der send(...)-Methode vor Ausnahmen zu schuetzen, denn die unterliegende Verbindung kann zwischen der Rueckgabe dieser Methode und dem Aufruf von send(...) getrennt werden.
		*/
		bool isConnected(bool refreshState = false);
		bool isDisconnected() const;
		bool isBound() const;
		bool isListening() const;

		/*
		Falls die instanz im Zustand Connected ist kontrolliert diese Methode, ob die unterliegende Verbindung noch besteht.
		Besteht die unterliegende Verbindung nicht mehr, so wird der Zustand auf Disconnected gesetzt und das SocketHandle, falls die Instanz es besitzt, geschlossen.
		*/
		void refreshState();


		/*
		Gibt die Anzahl der Bytes zurueck, die zum Empfangen ueber die Verbindung bereit stehen.
		Diese Methode wirft nur dann eine Ausnahme, wenn der Zustand der Socket nicht Connected ist, oder eine Betriebssystemfunktion einen Fehler erzeugt.
		*/
		uint32_t getReceivableBytes();
		/*
		Gibt zurueck, ob mindestens ein Bytes zum Empfangen ueber die Verbindung bereit steht.
		Diese Methode wirft nur dann eine Ausnahme, wenn der Zustand der Socket nicht Connected ist, oder eine Betriebssystemfunktion einen Fehler erzeugt.
		*/
		bool dataAvailable();

		Endpoint const& getRemoteEndpoint() const;
		Endpoint const& getLocalEndpoint() const;

		/*
		Wenn die Instanz das unterliegende SocketHandle nicht besitzt, so wird es weder im Destruktor, noch in der close()-Methode geschlossen.
		Dies erm�glicht das Nutzen der Methoden dieser Klasse zur Erstellung und Verwaltung eines Handles, welches danach f�r beliebige andere Zwecke unabh�ngig dieser Klasse genutzt werden kann.
		*/
		void setOwnsHandle(bool ownsHandle);
		SocketHandle getHandle();

	public:
		static SocketHandle CreateHandle();
	};
}

#endif
