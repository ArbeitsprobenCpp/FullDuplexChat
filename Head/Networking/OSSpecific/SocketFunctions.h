#ifndef NETWORKING_OSSPECIFIC_SOCKETFUNCTIONS_H
#define NETWORKING_OSSPECIFIC_SOCKETFUNCTIONS_H

//Interne, betriebssystemabhaengige Funktionen, die von den Klassen des Networking-Namespaces benoetigt werden.

#include <Networking/OSSpecific/SocketDefinitions.h>

#ifdef _WINDOWS

#define ISSOCKETERROR(s) (s == SOCKET_ERROR)
#define LASTSOCKETERROR WSAGetLastError()

#else

#include <errno.h>

#define ISSOCKETERROR(s) (s < 0)
#define LASTSOCKETERROR errno

#endif

namespace Networking
{
	inline SocketHandle socketCreate()
	{
#ifdef _WINDOWS
		static bool initialized(false);
		if (!initialized)
		{
			WSADATA w;
			int errcode;
			if ((errcode = WSAStartup(MAKEWORD(2,2), &w)) != 0)
				throw OSApiException("WinSock2 konnte nicht gestartet werden", errcode);
			else
				initialized = true;
		}
#endif

		SocketHandle rv = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
		if (ISSOCKETERROR(rv))
			throw OSApiException("Error creating socket handle", LASTSOCKETERROR);

		return rv;
	}

	inline void socketShutdown(SocketHandle h)
	{
		bool error;

#ifdef _WINDOWS
		error = shutdown(h, SD_BOTH) != 0;
#else
		error = shutdown(h, SHUT_RDWR) != 0;
#endif

		if (error)
			throw OSApiException("Error shutting down socket", LASTSOCKETERROR);
	}
	inline void socketClose(SocketHandle h)
	{
		bool error;

#ifdef _WINDOWS
		error = closesocket(h) != 0;
#else
		error = close(h) != 0;
#endif

		if (error)
			throw OSApiException("Error closing socket", LASTSOCKETERROR);
	}

	inline void fillSockaddr_in(sockaddr_in& addr_in, Endpoint const& ep)
	{
		addr_in.sin_family = AF_INET;
		addr_in.sin_port = htons(ep.getPort());
		IPv4Address const& ipAddress(static_cast<IPv4Address const&>(ep.getAddress()));
#ifdef _WINDOWS
		addr_in.sin_addr.S_un.S_addr = ipAddress.asBigEndian();
#else
		addr_in.sin_addr.s_addr = ipAddress.asBigEndian();
#endif
	}
	inline Endpoint extractEndpoint(sockaddr_in const& addr_in)
	{
#ifdef _WINDOWS
		IPv4Address address(IPv4Address::FromBigEndian(addr_in.sin_addr.S_un.S_addr));
#else
		IPv4Address address(IPv4Address::FromBigEndian(addr_in.sin_addr.s_addr));
#endif

		unsigned short port = ntohs(addr_in.sin_port);

		return Endpoint(address, port);
	}
	inline Endpoint determineLocalEndpoint(SocketHandle h)
	{
		sockaddr_in addr_in;
		sockaddr* address = reinterpret_cast<sockaddr*>(&addr_in);
		socklen_t size(sizeof(sockaddr_in));

		if (!ISSOCKETERROR(getsockname(h, address, &size)))
			return extractEndpoint(addr_in);
		else
			throw OSApiException("Could not retrieve local endpoint", LASTSOCKETERROR);
	}

	
	inline void socketConnect(SocketHandle h, Endpoint const& ep)
	{
		sockaddr_in addr_in;
		sockaddr* address = reinterpret_cast<sockaddr*>(&addr_in);
		socklen_t size(sizeof(sockaddr_in));
			
		fillSockaddr_in(addr_in, ep);

		if (ISSOCKETERROR(connect(h, address, size)))
			throw OSApiException("Connection failed", LASTSOCKETERROR);
	}
	inline void socketBind(SocketHandle h, Endpoint const& localEndpoint)
	{
		sockaddr_in addr_in;
		sockaddr* address = reinterpret_cast<sockaddr*>(&addr_in);
		socklen_t size(sizeof(sockaddr_in));

		fillSockaddr_in(addr_in, localEndpoint);

		if (ISSOCKETERROR(bind(h, address, size)))
			throw OSApiException("Bind failed", LASTSOCKETERROR);
	}
	inline void socketListen(SocketHandle h, int backlog)
	{
		if (ISSOCKETERROR(listen(h, backlog)))
			throw OSApiException("Failed to listen on socket", LASTSOCKETERROR);
	}
	inline SocketHandle socketAccept(SocketHandle h, Endpoint* remoteEndpointBuffer)
	{
		sockaddr_in addr_in;
		sockaddr* address = reinterpret_cast<sockaddr*>(&addr_in);
		socklen_t size(sizeof(sockaddr_in));

		SocketHandle newSocket = accept(h, address, &size);
		if (!ISSOCKETERROR(newSocket))
		{
			if (remoteEndpointBuffer)
				*remoteEndpointBuffer = extractEndpoint(addr_in);
			return newSocket;
		}
		else
			throw OSApiException("Could not accept connection", LASTSOCKETERROR);
	}
	inline int socketReceive(SocketHandle h, char* buffer, int bufferSize)
	{
		int amount = recv(h, buffer, bufferSize, 0);
#ifndef _WINDOWS
		//Manche Linuxversionen geben einen Fehler zurueck mit errno 104 (Connection reset by peer), wenn die Verbindung geschlossen wurde
		if (ISSOCKETERROR(amount) && LASTSOCKETERROR == 104)
			amount = 0;
#endif
		if (ISSOCKETERROR(amount))
			throw OSApiException("Error receiving from socket", LASTSOCKETERROR);

		return amount;
	}
	inline int socketSend(SocketHandle h, char* buffer, int length)
	{
		int rv = send(h, buffer, length, 0);
		if (ISSOCKETERROR(rv))
			throw OSApiException("Could not send data", LASTSOCKETERROR);
		return rv;
	}
	inline u_long socketReceivableBytes(SocketHandle h)
	{
		u_long availableData;

#ifdef _WINDOWS
		int returnvalue = ioctlsocket(h, FIONREAD, &availableData);
#else
		int returnvalue = ioctl(h, FIONREAD, &availableData);
#endif

		if (ISSOCKETERROR(returnvalue))
			throw OSApiException("Error checking available bytes", LASTSOCKETERROR);

		return availableData;
	}
	inline bool socketCheckConnected(SocketHandle h)
	{
		fd_set chk;
		FD_ZERO(&chk);
		FD_SET(h, &chk);

		timeval t;
		t.tv_sec = 0;
		t.tv_usec = 0;

		int result = select(static_cast<int>(h) + 1, &chk, NULL, &chk, &t);
		if (!ISSOCKETERROR(result))
		{
			if (result > 0 && FD_ISSET(h, &chk))
				return socketReceivableBytes(h) > 0; //Socket wurde aktualisiert - entweder weil sie nicht verbunden ist, oder weil Daten verfuegbar sind

			return true; //Zeitlimit abgelaufen => Die Socket ist noch verbunden, hat aber nichts empfangen
		}
		else
			throw OSApiException("Error refreshing socket state", LASTSOCKETERROR);
	}
}


#endif