#ifndef NETWORKING_OSSPECIFIC_SOCKETDEFINITIONS_H
#define NETWORKING_OSSPECIFIC_SOCKETDEFINITIONS_H

//Oeffentliche, betriebssystemabhaengige Definitionen, die fuer den Networking-Namespace benoetigt werden.

#include <Base.h>


#ifdef _WINDOWS

#include <WinSock2.h>

typedef int socklen_t;

#else

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/ioctl.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <fcntl.h>

#endif

namespace Networking
{
#ifdef _WINDOWS
	typedef SOCKET SocketHandle;
#else
	typedef int SocketHandle;
#endif
}


#endif