#ifndef STRINGHELPERS_H
#define STRINGHELPERS_H

#include <Base.h>

#include <sstream>
#include <locale>
#include <vector>

class StringHelpers
{
public:
	//Es sollten Spezialisierungen von ToString<T> fuer Zahlentypen implementiert werden, die std::to_string nutzen.
	template<typename T>
	static inline std::string ToString(T data)
	{
		std::stringstream ss;
		ss << data;
		return ss.str();
	}
	static inline std::string ToString(std::exception const& ex)
	{
		return ex.what();
	}
	static inline std::string ToString(char const* bytes, uint32_t length)
	{
		std::string rv;
		rv.reserve(length);

		char buf[512];
		int offset = 0;
		while (length > 0)
		{
			uint32_t i = 0;
			for (; i < (sizeof(buf) - 1) && i < length; i++)
				buf[i] = bytes[i + offset];
			buf[i] = 0;

			rv += buf;
			length -= i;
			offset += i;
		}

		return rv;
	}

	template<typename T>
	static inline T Parse(std::string const& str)
	{
		T out;

		std::istringstream ss(str);
		ss >> out;
		//ss.fail ist true wenn der Typ T nicht passt
		//!ss.eof() ist true wenn danach ungueltige Zeichen folgen
		if (ss.fail() || !ss.eof())
			throw Exception(str + std::string(" could not be parsed"));

		return out;
	}

	static inline std::string Trim(std::string const& str)
	{
		char const* start = str.data();
		char const* end = str.data() + str.length() - 1;
		while (start < end && start[0] == ' ')
			start++;
		while (start < end && end[0] == ' ')
			end--;

		return str.substr(start - str.data(), (end - start) + 1);
	}

	static inline bool Equals(std::string const& a, std::string const& b, bool caseSensitive = true)
	{
		if (caseSensitive)
			return a == b;
		else
		{
			if (a.length() == b.length())
			{
				char const* ad(a.data());
				char const* bd(b.data());

				for (std::string::size_type i = 0; i < a.length(); i++)
				{
					if (tolower(ad[i]) != tolower(bd[i]))
						return false;
				}
				return true;
			}
			else
				return false;
		}
	}

	static inline std::string ToLower(std::string const& str)
	{
		std::string rv;
		rv.reserve(str.length());

		char const* data(str.data());
		for (std::string::size_type i = 0; i < str.length(); i++)
			rv += tolower(data[i]);

		return rv;
	}

	static inline std::string Replace(std::string const& base, std::string const& toReplace, std::string const& with, bool replaceAll = true)
	{
		std::string::size_type offset = 0;
		std::string::size_type index;

		std::string rv;

		while ((index = base.find(toReplace, offset)) != base.npos)
		{
			rv += base.substr(offset, index - offset);
			rv += with;
			offset = index + toReplace.size();

			if (!replaceAll)
				break;
		}

		return rv + base.substr(offset);
	}

	static inline std::vector<std::string> Split(std::string const& base, std::string const& pattern, bool removeEmptyEntries = false)
	{
		std::vector<std::string> rv;

		std::string tmp(base);
		std::string::size_type idx;
		while ((idx = tmp.find(pattern)) != tmp.npos)
		{
			if (!removeEmptyEntries || idx != 0)
				rv.push_back(tmp.substr(0, idx));
			tmp = tmp.substr(idx + pattern.size());
		}
		if (!removeEmptyEntries || tmp.size() > 0)
			rv.push_back(tmp);

		return rv;
	}
};
template<>
inline std::string StringHelpers::ToString<std::string const&>(std::string const& data)
{
	return data;
}

#endif