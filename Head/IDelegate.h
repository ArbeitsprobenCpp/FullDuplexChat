#ifndef IDELEGATE_H
#define IDELEGATE_H

/*
Erlaeuterung zu Delegaten in Delegate.h
*/

#include <Base.h>

template<typename R, typename P>
class IDelegate
{
public:
	virtual ~IDelegate() {}

public:
	virtual R operator()(P) const = 0;
	virtual bool operator==(IDelegate<R, P> const&) const = 0;
	virtual bool operator!=(IDelegate<R, P> const&) const = 0;

public:
	virtual IDelegate* copy() const = 0;

	virtual bool isNull() const = 0;
};
template<typename P>
class IDelegate<void, P>
{
public:
	virtual ~IDelegate() {}

public:
	virtual void operator()(P) const = 0;
	virtual bool operator==(IDelegate<void, P> const&) const = 0;
	virtual bool operator!=(IDelegate<void, P> const&) const = 0;

public:
	virtual IDelegate* copy() const = 0;

	virtual bool isNull() const = 0;
};

template<typename R>
class IDelegate<R, void>
{
public:
	virtual ~IDelegate() {}

public:
	virtual R operator()() const = 0;
	virtual bool operator==(IDelegate<R, void> const&) const = 0;
	virtual bool operator!=(IDelegate<R, void> const&) const = 0;

public:
	virtual IDelegate* copy() const = 0;

	virtual bool isNull() const = 0;
};

template<>
class IDelegate<void, void>
{
public:
	virtual ~IDelegate() {}

public:
	virtual void operator()() const = 0;
	virtual bool operator==(IDelegate<void, void> const&) const = 0;
	virtual bool operator!=(IDelegate<void, void> const&) const = 0;

public:
	virtual IDelegate* copy() const = 0;

	virtual bool isNull() const = 0;
};


#endif