#ifndef BASE_H
#define BASE_H

#define NOMINMAX

#ifdef _WIN32
#ifndef _WINDOWS
#define _WINDOWS
#endif
#endif

#include <stdint.h>
#include <string>
#include <stdexcept>

#ifndef NULL
#define NULL ((void*)0)
#endif

class Exception : public std::runtime_error
{
   private:
      Exception const* innerException;

   public:
      Exception(std::string const& message)
          :std::runtime_error(message)
      {
      }
      virtual ~Exception() throw ()
      {
      }
};

class OSApiException : public Exception
{
public:
    typedef int ErrorCode;

private:
    ErrorCode errorCode;

public:
    OSApiException(std::string const& message, ErrorCode error);
    OSApiException(ErrorCode error);

public:
    ErrorCode getErrorCode() const;
};

class TimeoutException : public Exception
{
public:
	TimeoutException()
		:Exception("A Timeout occurred")
	{
	}
};

#endif