#ifndef DELEGATEFUNCTION_H
#define DELEGATEFUNCTION_H

/*
Erlaeuterung zu Delegaten in Delegate.h
*/

#include <IDelegate.h>

template<typename R, typename P>
class DelegateFunction : public IDelegate<R, P>
{
public:
	typedef R (* Function)(P);
private:
	Function function;

public:
	DelegateFunction(Function function)
		:function(function)
	{
	}

	DelegateFunction()
		:function(NULL)
	{
	}

	DelegateFunction(DelegateFunction const& original)
		:function(original.function)
	{
	}

	virtual ~DelegateFunction() {}

public:
	DelegateFunction& operator=(DelegateFunction const& original)
	{
		this->function = original.function;
		return *this;
	}
	R operator()(P parameter) const
	{
		if (this->function)
			return function(parameter);
		else
			throw Exception("DelegateFunction is null");
	}
	virtual bool operator==(IDelegate<R, P> const& other) const
	{
		DelegateFunction<R, P> const* dm = dynamic_cast<DelegateFunction<R, P> const*>(&other);
		return dm && dm->function == this->function;
	}
	virtual bool operator!=(IDelegate<R, P> const& other) const
	{
		return !this->operator==(other);
	}

public:
	IDelegate<R, P>* copy() const
	{
		return new DelegateFunction<R, P>(*this);
	}

	bool isNull() const
	{
		return this->function == NULL;
	}
};
template<typename P>
class DelegateFunction<void, P> : public IDelegate<void, P>
{
public:
	typedef void (* Function)(P);
private:
	Function function;

public:
	DelegateFunction(Function function)
		:function(function)
	{
	}

	DelegateFunction()
		:function(NULL)
	{
	}

	DelegateFunction(DelegateFunction const& original)
		:function(original.function)
	{
	}

	virtual ~DelegateFunction() {}

public:
	DelegateFunction& operator=(DelegateFunction const& original)
	{
		this->function = original.function;
		return *this;
	}
	void operator()(P parameter) const
	{
		if (this->function)
			function(parameter);
		else
			throw Exception("DelegateFunction is null");
	}
	virtual bool operator==(IDelegate<void, P> const& other) const
	{
		DelegateFunction<void, P> const* dm = dynamic_cast<DelegateFunction<void, P> const*>(&other);
		return dm && dm->function == this->function;
	}
	virtual bool operator!=(IDelegate<void, P> const& other) const
	{
		return !this->operator==(other);
	}

public:
	IDelegate<void, P>* copy() const
	{
		return new DelegateFunction(*this);
	}

	bool isNull() const
	{
		return this->function == NULL;
	}
};

template<typename R>
class DelegateFunction<R, void> : public IDelegate<R, void>
{
public:
	typedef R (* Function)(void);
private:
	Function function;

public:
	DelegateFunction(Function function)
		:function(function)
	{
	}

	DelegateFunction()
		:function(NULL)
	{
	}

	DelegateFunction(DelegateFunction const& original)
		:function(original.function)
	{
	}

	virtual ~DelegateFunction() {}

public:
	DelegateFunction& operator=(DelegateFunction const& original)
	{
		this->function = original.function;

		return *this;
	}
	R operator()() const
	{
		if (this->function)
			return function();
		else
			throw Exception("DelegateFunction is null");
	}
	virtual bool operator==(IDelegate<R, void> const& other) const
	{
		DelegateFunction<R, void> const* dm = dynamic_cast<DelegateFunction<R,void> const*>(&other);
		return dm && dm->function == this->function;
	}
	virtual bool operator!=(IDelegate<R, void> const& other) const
	{
		return !this->operator==(other);
	}

public:
	IDelegate<R, void>* copy() const
	{
		return new DelegateFunction(*this);
	}

	bool isNull() const
	{
		return this->function == NULL;
	}
};

template<>
class DelegateFunction<void, void> : public IDelegate<void, void>
{
public:
	typedef void (* Function)(void);
private:
	Function function;

public:
	DelegateFunction(Function function)
		:function(function)
	{
	}

	DelegateFunction()
		:function(NULL)
	{
	}

	DelegateFunction(DelegateFunction const& original)
		:function(original.function)
	{
	}

	virtual ~DelegateFunction() {}

public:
	DelegateFunction& operator=(DelegateFunction const& original)
	{
		this->function = original.function;
		return *this;
	}
	void operator()() const
	{
		if (this->function)
			function();
		else
			throw Exception("DelegateFunction is null");
	}
	virtual bool operator==(IDelegate<void, void> const& other) const
	{
		DelegateFunction<void, void> const* dm = dynamic_cast<DelegateFunction<void, void> const*>(&other);
		return dm && dm->function == this->function;
	}
	virtual bool operator!=(IDelegate<void, void> const& other) const
	{
		return !this->operator==(other);
	}

public:
	IDelegate<void,void>* copy() const
	{
		return new DelegateFunction(*this);
	}

	bool isNull() const
	{
		return this->function == NULL;
	}
};

#endif