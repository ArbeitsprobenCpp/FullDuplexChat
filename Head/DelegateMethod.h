#ifndef DELEGATEMETHOD_H
#define DELEGATEMETHOD_H

/*
Erlaeuterung zu Delegaten in Delegate.h
*/

#include <IDelegate.h>

template<class C, typename R, typename P>
class DelegateMethod : public IDelegate<R, P>
{
public:
	typedef R (C::* Method)(P);

private:
	C* instance;
	Method method;

public:
	DelegateMethod(C& instance, Method method)
		:instance(&instance),
		method(method)
	{
	}

	DelegateMethod()
		:instance(NULL),
		method(NULL)
	{
	}

	DelegateMethod(DelegateMethod const& original)
		:instance(original.instance),
		method(original.method)
	{
	}

	virtual ~DelegateMethod() {}

public:
	DelegateMethod& operator=(DelegateMethod const& original)
	{
		this->instance = original.instance;
		this->method = original.method;
		return *this;
	}

	R operator()(P parameter) const
	{
		if (this->method)
			return (instance->*method)(parameter);
		else
			throw Exception("DelegateMethod is null");
	}
	virtual bool operator==(IDelegate<R, P> const& other) const
	{
		DelegateMethod<C, R, P> const* dm = dynamic_cast<DelegateMethod<C, R, P> const*>(&other);
		return dm && dm->instance == this->instance && dm->method == this->method;
	}
	virtual bool operator!=(IDelegate<R, P> const& other) const
	{
		return !this->operator==(other);
	}

public:
	IDelegate<R, P>* copy() const
	{
		return new DelegateMethod(*this);
	}

	bool isNull() const
	{
		return this->method == NULL;
	}
};
template<typename C, typename P>
class DelegateMethod<C, void, P> : public IDelegate<void, P>
{
public:
	typedef void (C::* Method)(P);

private:
	C* instance;
	Method method;

public:
	DelegateMethod(C& instance, Method method)
		:instance(&instance),
		method(method)
	{
	}

	DelegateMethod()
		:instance(NULL),
		method(NULL)
	{
	}

	DelegateMethod(DelegateMethod const& original)
		:instance(original.instance),
		method(original.method)
	{
	}

	virtual ~DelegateMethod() {}

public:
	DelegateMethod& operator=(DelegateMethod const& original)
	{
		this->instance = original.instance;
		this->method = original.method;

		return *this;
	}

	void operator()(P parameter) const
	{
		if (this->method)
			(instance->*method)(parameter);
		else
			throw Exception("DelegateMethod is null");
	}
	virtual bool operator==(IDelegate<void, P> const& other) const
	{
		DelegateMethod<C, void, P> const* dm = dynamic_cast<DelegateMethod<C, void, P> const*>(&other);
		return dm && dm->instance == this->instance && dm->method == this->method;
	}
	virtual bool operator!=(IDelegate<void, P> const& other) const
	{
		return !this->operator==(other);
	}

public:
	IDelegate<void, P>* copy() const
	{
		return new DelegateMethod(*this);
	}

	bool isNull() const
	{
		return this->method == NULL;
	}
};

template<typename C, typename R>
class DelegateMethod<C, R, void> : public IDelegate<R, void>
{
public:
	typedef R (C::* Method)(void);

private:
	C* instance;
	Method method;

public:
	DelegateMethod(C& instance, Method method)
		:instance(&instance),
		method(method)
	{
	}

	DelegateMethod()
		:instance(NULL),
		method(NULL)
	{
	}

	DelegateMethod(DelegateMethod const& original)
		:instance(original.instance),
		method(original.method)
	{
	}

	virtual ~DelegateMethod() {}

public:
	DelegateMethod& operator=(DelegateMethod const& original)
	{
		this->instance = original.instance;
		this->method = original.method;

		return *this;
	}

	R operator()() const
	{
		if (this->method)
			return (instance->*method)();
		else
			throw Exception("DelegateMethod is null");
	}
	virtual bool operator==(IDelegate<R, void> const& other) const
	{
		DelegateMethod<C, R, void> const* dm = dynamic_cast<DelegateMethod<C, R, void> const*>(&other);
		return dm && dm->instance == this->instance && dm->method == this->method;
	}
	virtual bool operator!=(IDelegate<R, void> const& other) const
	{
		return !this->operator==(other);
	}

public:
	IDelegate<R, void>* copy() const
	{
		return new DelegateMethod(*this);
	}

	bool isNull() const
	{
		return this->method == NULL;
	}
};

template<typename C>
class DelegateMethod<C, void, void> : public IDelegate<void, void>
{
public:
	typedef void (C::* Method)(void);

private:
	C* instance;
	Method method;

public:
	DelegateMethod(C& instance, Method method)
		:instance(&instance),
		method(method)
	{
	}

	DelegateMethod()
		:instance(NULL),
		method(NULL)
	{
	}

	DelegateMethod(DelegateMethod const& original)
		:instance(original.instance),
		method(original.method)
	{
	}

	virtual ~DelegateMethod() {}

public:
	DelegateMethod& operator=(DelegateMethod const& original)
	{
		this->instance = original.instance;
		this->method = original.method;

		return *this;
	}

	void operator()() const
	{
		if (this->method)
			(instance->*method)();
		else
			throw Exception("DelegateMethod is null");
	}
	virtual bool operator==(IDelegate<void, void> const& other) const
	{
		DelegateMethod<C, void, void> const* dm = dynamic_cast<DelegateMethod<C, void, void> const*>(&other);
		return dm && dm->instance == this->instance && dm->method == this->method;
	}
	virtual bool operator!=(IDelegate<void, void> const& other) const
	{
		return !this->operator==(other);
	}

public:
	IDelegate<void, void>* copy() const
	{
		return new DelegateMethod<C, void, void>(*this);
	}

	bool isNull() const
	{
		return this->method == NULL;
	}
};

#endif