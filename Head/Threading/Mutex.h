#ifndef THREADING_MUTEX_H
#define THREADING_MUTEX_H

#include <Base.h>
#include <Threading/SynchronizationContext.h>
#include <Threading/OSSpecific/ThreadingDefinitions.h>


//Makros zur einfachen Nutzung von Mutexen in ueblichen Faellen
#define THREADING_SYNCHRONIZE(mutex) Threading::MutexSynchronizer synchronized(mutex)
#define THREADING_LOCK(mutex)		mutex.lock();\
									try
#define THREADING_RELEASE(mutex)	catch (...)\
									{\
										mutex.release();\
										throw;\
									}\
									mutex.release()


namespace Threading
{
	/*
	Diese Klasse nutzt native Mutexe bezüglich pthreads bzw. Windows Threads.

	Wenn die Thread-Klasse auf std::thread umgestellt wurde sollte diese Klasse entweder auf Nutzung von std::mutex umgestellt oder sogar entfernt werden. (C++11)
	*/
	class Mutex
	{
	private:
		MutexHandle handle;
		
		bool initializationError;
		int initializationErrorCode;

	public:
		Mutex();
		~Mutex();

	private:
		Mutex(Mutex const&);
		Mutex& operator=(Mutex const&);

    public:
		void lock();
		void release();
	};

	typedef SynchronizationContext<Mutex> MutexSynchronizer;
}

#endif