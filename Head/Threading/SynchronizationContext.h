#ifndef THREADING_SYNCHRONIZATIONCONTEXT_H
#define THREADING_SYNCHRONIZATIONCONTEXT_H

#include <Base.h>

namespace Threading
{
	/*
	Eine einfache Hilfsklasse die im Konstruktor einen Synchronisierungsmechanismus in Besitz nimmt und ihn im Destruktor wieder freigibt.
	Wird diese Klasse im Stack einer Funktion instanziert so wird der Synchronisierungsmechanismus automatisch bei Rueckgaben, Ausnahmen oder beim Verlassen der Methode freigegeben.
	*/
	template<class T>
	class SynchronizationContext
	{
	private:
		T& synchronizer;

	public:
		SynchronizationContext(T& synchronizer);
		~SynchronizationContext();
	};
}

#endif