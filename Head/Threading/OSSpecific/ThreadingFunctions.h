#ifndef THREADING_OSSPECIFIC_THREADINGFUNCTIONS_H
#define THREADING_OSSPECIFIC_THREADINGFUNCTIONS_H

//Interne, betriebssystemabhaengige Funktionen, die von den Klassen des Threading-Namespaces benoetigt werden.

#include <Threading/OSSpecific/ThreadingDefinitions.h>

#ifndef _WINDOWS

#include <errno.h>
#include <unistd.h>
#include <time.h>

#endif

namespace Threading
{
#ifndef _WINDOWS
	inline void fillTimespecForTimeout(timespec* timeoutSpec, DWORD timeoutMS)
	{
		const long NanosecondsPerSecond = 1000000000L;
		const long NanosecondsPerMillisecond = 1000000L;
		const long MillisecondsPerSecond = 1000L;


		if (clock_gettime(CLOCK_REALTIME, timeoutSpec) != 0)
			throw OSApiException("Error reading system clock", errno);

		timeoutSpec->tv_sec += static_cast<time_t>(timeoutMS / MillisecondsPerSecond);
		timeoutSpec->tv_nsec += (timeoutMS % MillisecondsPerSecond) * NanosecondsPerMillisecond;
		if (timeoutSpec->tv_nsec >= NanosecondsPerSecond)
		{
			timeoutSpec->tv_sec++;
			timeoutSpec->tv_nsec -= NanosecondsPerSecond;
		}
	}
#endif


	inline void mutexCreate(MutexHandle* buffer)
	{
#ifdef _WINDOWS
        *buffer = CreateMutex(0, false, 0);
        if ((*buffer) == NULL)
			throw OSApiException("Mutex could not be created", GetLastError());
#else
		//Make mutex recursive
		pthread_mutexattr_t attr;
		pthread_mutexattr_init(&attr);
		pthread_mutexattr_settype(&attr, PTHREAD_MUTEX_RECURSIVE);
        int returnvalue = pthread_mutex_init(buffer, &attr);
        if (returnvalue != 0)
			throw new OSApiException("Mutex could not be created", errno);
#endif
	}
	inline void mutexFreeHandle(MutexHandle& h)
	{
#ifdef _WINDOWS
        if (!CloseHandle(h))
			throw OSApiException("Error closing mutex handle", GetLastError());
#else
		int rv = pthread_mutex_destroy(&h);
        if (rv != 0)
			throw OSApiException("Error closing mutex handle", rv);
#endif
	}
	inline void mutexLock(MutexHandle& h, DWORD timeoutMS)
	{
#ifdef _WINDOWS
		switch (WaitForSingleObject(h, timeoutMS))
		{
		case WAIT_OBJECT_0:
		case WAIT_ABANDONED: //Im Fall von Abandoned bekommt der aufrufende Thread zwar den Mutex, aber es ist auf jeden Fall etwas nicht so gelaufen wie erwartet - Ausnahme werfen?
			//Erfolg
			break;
		case WAIT_TIMEOUT:
			throw TimeoutException();
			break;
		case WAIT_FAILED:
			throw OSApiException("Error locking mutex", GetLastError());
			break;
		}
#else
		int rv;
		if (timeoutMS != INFINITE)
		{
			timespec timeout;
			fillTimespecForTimeout(&timeout, timeoutMS);

			rv = pthread_mutex_timedlock(&h, &timeout);
		}
		else
			rv = pthread_mutex_lock(&h);

		if (rv != 0)
		{
			if (rv == ETIMEDOUT)
				throw TimeoutException();
			else
				throw OSApiException("Error locking mutex", rv);
		}
#endif
	}
	inline void mutexRelease(MutexHandle& h)
	{
#ifdef _WINDOWS
        if (!ReleaseMutex(h))
			throw OSApiException("Error releasing mutex", GetLastError());
#else
		int rv = pthread_mutex_unlock(&h);
        if (rv != 0)
			throw OSApiException("Error releasing mutex", rv);
#endif
	}






	
	inline ThreadHandle threadStart(void* (*startFunction)(void*), void* args)
	{
		ThreadHandle h;
#ifdef _WINDOWS
		h = CreateThread(0, 2000, (LPTHREAD_START_ROUTINE)startFunction, args, 0, NULL);
		if (h == NULL)
			throw OSApiException("Error creating thread", GetLastError());
#else
		if (pthread_create(&h, NULL, startFunction, args) != 0)
			throw OSApiException("Error creating thread", errno);
#endif
		return h;
	}
	inline void threadTerminate(ThreadHandle h)
	{
#ifdef _WINDOWS
		if (!TerminateThread(h, 0))
			throw OSApiException("Error terminating thread", GetLastError());
#else
		int rv = pthread_cancel(h);
		if (rv != 0)
			throw OSApiException("Error terminating thread", rv);
#endif
	}
	inline void threadFreeHandle(ThreadHandle h)
	{
#ifdef _WINDOWS
        if (!CloseHandle(h))
			throw OSApiException("Error closing thread handle", GetLastError());
#else
		//pthread_join schliesst automatisch das zugehoerige ThreadHandle
#endif
	}
#ifndef _WINDOWS
	/*
	Da pthread_join keine Moeglichkeit bietet, einen Timeout anzugeben, wird hier ein Workaround implementiert.
	Dazu wird ein weiterer Thread erstellt, der pthread_join ausfuehrt.
	Der urspruengliche Thread wartet waehrenddessen auf einen Mutex und bricht den join-Thread ab, wenn der Timeout ablaeuft.
	*/
	struct JoinArgs
	{
		MutexHandle Mutex;
		ThreadHandle Thread;
		bool Joined;
		int Error;
	};
	static void* pthread_join_timeout_helper(void* args)
	{
		JoinArgs* joinArgs = static_cast<JoinArgs*>(args);
		
		mutexLock(joinArgs->Mutex, INFINITE);

		joinArgs->Error = pthread_join(joinArgs->Thread, NULL);
		joinArgs->Joined = true;

		mutexRelease(joinArgs->Mutex);

		return NULL;
	}
	inline void pthread_join_timeout(ThreadHandle thread, DWORD timeoutMS)
	{
		if (timeoutMS != INFINITE)
		{
			JoinArgs joinArgs;
			mutexCreate(&joinArgs.Mutex);
			joinArgs.Thread = thread;
			joinArgs.Joined = false;
			joinArgs.Error = 0;

			ThreadHandle joinThread = threadStart(&pthread_join_timeout_helper, &joinArgs);

			timespec timeout;
			fillTimespecForTimeout(&timeout, timeoutMS);

			bool joined = false;
			int error = 0;
			do
			{
				//Dem joinThread die Chance geben, den Mutex zu nehmen
				sched_yield();
				
				error = pthread_mutex_timedlock(&joinArgs.Mutex, &timeout);
				if (error != 0)
					break;

				joined = joinArgs.Joined;

				mutexRelease(joinArgs.Mutex);
			} while (!joined);

			try
			{
				mutexFreeHandle(joinArgs.Mutex);
			}
			catch (OSApiException& ex)
			{
				if (error == 0)
					error = ex.getErrorCode();
				//else: Es ist bereits ein Fehler bzgl des Mutex aufgetreten. Der zweite Fehler wird ignoriert, da er wahrscheinlich durch den ersten Fehler verursacht wurde oder beide die selbe Fehlerquelle haben.
			}

			if (error != 0 || joinArgs.Error != 0)
			{
				try
				{
					threadTerminate(joinThread);
				}
				catch (...)
				{
				}
				pthread_join(joinThread, NULL);
			}

			if (error != 0)
			{
				if (error == ETIMEDOUT)
					throw TimeoutException();
				else
					throw OSApiException("Mutex error while waiting for thread termination", error);
			}
			if (joinArgs.Error != 0)
				throw OSApiException("Error waiting for thread termination", joinArgs.Error);
		}
		else
			pthread_join(thread, NULL);
	}
#endif
	inline void threadWaitForTermination(ThreadHandle h, DWORD timeoutMS)
	{
#ifdef _WINDOWS
		switch (WaitForSingleObject(h, timeoutMS))
		{
		case WAIT_OBJECT_0:
			//Erfolg
			break;
		case WAIT_TIMEOUT:
			throw TimeoutException();
			break;
		case WAIT_FAILED:
			throw OSApiException("Error waiting for thread termination", GetLastError());
			break;
		}
#else
		pthread_join_timeout(h, timeoutMS);
#endif
	}

	inline void threadSleep(uint32_t ms)
	{
#ifdef _WINDOWS
		while (ms > MAXDWORD)
		{
			Sleep(MAXDWORD);
			ms -= MAXDWORD;
		}
		Sleep(static_cast<DWORD>(ms));
#else
		timespec timeout;
		timeout.tv_sec = static_cast<time_t>(ms / 1000);
		timeout.tv_nsec = ms - (timeout.tv_sec * 1000);
		timeout.tv_nsec *= 1000000L;

		timespec remaining = {0, 0};

		timespec* req = &timeout;
		timespec* rem = &remaining;

		do
		{
			int rv = nanosleep(req, rem);
			if (rv == 0)
				break;
			else
			{
				int error = errno;
				if (errno == EINTR)
				{
					//Ein Signal ist eingegangen, Sleep fortsetzen
					timespec* tmp = req;
					req = rem;
					rem = tmp;
				}
				else
					throw OSApiException("Error while sleeping", error);
			}
		} while (true);
#endif
	}

	inline ThreadId threadGetCurrentId()
	{
#ifdef _WINDOWS
		return GetCurrentThreadId();
#else
		return pthread_self();
#endif
	}
}

#endif