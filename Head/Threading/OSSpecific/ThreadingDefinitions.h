#ifndef THREADING_OSSPECIFIC_THREADINGDEFINITIONS_H
#define THREADING_OSSPECIFIC_THREADINGDEFINITIONS_H

//Oeffentliche, betriebssystemabhaengige Definitionen, die fuer den Threading-Namespace benoetigt werden.

#include <Base.h>
#include <Delegate.h>


#ifdef _WINDOWS

#define WIN32_LEAN_AND_MEAN
#include <Windows.h>

#else

#include <pthread.h>
#include <semaphore.h>

#define DWORD uint32_t
#define INFINITE 0xFFFFFFFF

#endif

namespace Threading
{
#ifdef _WINDOWS
    typedef HANDLE ThreadHandle;
	typedef DWORD ThreadId;
#else
    typedef pthread_t ThreadHandle;
	typedef pthread_t ThreadId;
#endif

	typedef Delegate<void*, void*> ThreadStartRoutine;




#ifdef _WINDOWS
	typedef HANDLE MutexHandle;
#else
	typedef pthread_mutex_t MutexHandle;
#endif
}

#endif