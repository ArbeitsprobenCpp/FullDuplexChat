#ifndef THREADING_THREAD_H
#define THREADING_THREAD_H

#include <map>
#include <vector>

#include <Threading/OSSpecific/ThreadingDefinitions.h>
#include <Threading/Mutex.h>


namespace Threading
{
	/*
	Diese Klasse nutzt je nach Umgebung pthreads oder native Windows Threads.

	Das Entfernen dieser Klasse um stattdessen std::thread (C++11) zu nutzen ist nicht zwingend sinnvoll, da diese Klasse zus�tzliche Features bietet.
	Ein Umbau, sodass intern std::thread statt nativer Threads genutzt werden w�re potentiell sinnvoll, w�rde aber die Unterst�tzung �lterer Systeme/Compiler entfernen.
	*/
    class Thread
    {
    private:
        struct ThreadStartArgs
        {
            Thread* Threadobject;
            void* ActualArgs;
        };


    public:
		typedef Delegate<void, Thread*> ThreadEndHandler;
		typedef std::vector<ThreadEndHandler> ThreadEndHandlers;

	private:
		typedef std::map<ThreadId, Thread*> Threadlist;

    private:
		bool running;
		bool freed;

        ThreadHandle threadHandle;
		ThreadId threadId;

        ThreadStartRoutine function;
		ThreadEndHandlers endHandlers; 
		Mutex handlerLock;

    private:
        Thread(Thread const & original);
        Thread& operator=(Thread const & original);

    private:
        void freeHandle();

        void assertRunning() const;
		void assertNotRunning() const;

    public:
        Thread(ThreadStartRoutine function);
        ~Thread();

		/*
		Fuegt ein Callback hinzu, das aufgerufen wird, bevor der durch dieses Objekt repraesentierte Thread endet.
		Diese Callbacks werden im repraesentierten Thread aufgerufen, bevor er terminiert.
		*/
		void addThreadEndHandler(ThreadEndHandler const&);
		void removeThreadEndHandler(ThreadEndHandler const&);

        void start(void* args = NULL);
        void stop();

        bool isRunning() const;

        void waitForTermination(DWORD timeoutMS = INFINITE) const;

	private:
		static Mutex threadlistLock;
		static Threadlist threads;

    private:
        static void* threadStartFunction(void* args);
            
	public:
		static const ThreadId NoThread;

	public:
        static void Sleep(uint32_t ms);
		/*
		Gibt die ThreadId des aufrufenden Threads zurueck.
		*/
		static ThreadId CurrentId();
		/*
		Gibt das zum aufrufenden Thread gehoerende Thread-Objekt zurueck.
		Falls der aufrufende Thread nicht ueber diese Klasse erstellt wurde wird NULL zurueckgegeben.
		*/
		static Thread* Current();
    };
}

#endif
