#ifndef DELEGATE_H
#define DELEGATE_H

/*
Delegaten sind aufrufbare Routinen (globale Funktion, statische Methode oder Instanzmethode).
Diese Routine kann einen optionalen, beliebigen Rueckgabetypen R und einen optionalen, beliebigen Parametertypen P haben.

IDelegate<R, P> ist eine abstrakte Klasse, von der alle anderen Delegaten erben. Sie ist ein Interface, das die Methoden vorgibt.
DelegateFunction<R, P> ist die konkrete Umsetzung fuer globale Funktionen und statische Methoden.
DelegateMethod<C, R, P> ist die konkrete Umsetzung fuer Instanzmethoden der Klasse C.
Delegate<R, P> ist ein Wrapper fuer DelegateFunction und DelegateMethod, der es erlaubt einen beliebige Delegaten als Werttyp zu uebergeben, statt auf einen IDelegate-Pointer zurueckgreifen zu muessen.
*/

#include <DelegateFunction.h>
#include <DelegateMethod.h>

template<typename R, typename P>
class Delegate : public IDelegate<R, P>
{
private:
	IDelegate<R, P>* delegat;

public:
	Delegate()
		:delegat(NULL)
	{
	}

	Delegate(IDelegate<R, P> const& original)
		:delegat(original.copy())
	{
	}

	template<class C>
	Delegate(DelegateMethod<C, R, P> const& original)
		:delegat(new DelegateMethod<C, R, P>(original))
	{
	}

	Delegate(DelegateFunction<R, P> const& original)
		:delegat(new DelegateFunction<R, P>(original))
	{
	}

	Delegate(Delegate const& original)
		:delegat(NULL)
	{
		if (original.delegat)
			delegat = original.delegat->copy();
	}
	virtual ~Delegate()
	{
		destroy();
	}


public:
	Delegate& operator=(Delegate const& original)
	{
		destroy();

		if (original.delegat)
			delegat = original.delegat->copy();

		return *this;
	}

	template<class C>
	Delegate& operator=(DelegateMethod<C, R, P> const& original)
	{
		destroy();

		delegat = new DelegateMethod<C, R, P>(original);

		return *this;
	}

	Delegate& operator=(DelegateFunction<R, P> const& original)
	{
		destroy();

		delegat = new DelegateFunction<R, P>(original);

		return *this;
	}

	R operator()(P parameter) const
	{
		if (delegat)
			return delegat->operator()(parameter);
		else
			throw Exception("Delegate does not have value");
	}
	bool operator==(IDelegate<R, P> const& other) const
	{
		return this->delegat->operator==(other);
	}
	bool operator!=(IDelegate<R, P> const& other) const
	{
		return this->delegat->operator!=(other);
	}

private:
	void destroy()
	{
		if (delegat)
		{
			delete delegat;
			delegat = NULL;
		}
	}

public:
	IDelegate<R, P>* copy() const
	{
		return new Delegate(*this);
	}


	bool isNull() const
	{
		return this->delegat == NULL || this->delegat->isNull();
	}
};
template<typename P>
class Delegate<void, P> : public IDelegate<void, P>
{
private:
	IDelegate<void, P>* delegat;

public:
	Delegate()
		:delegat(NULL)
	{
	}

	Delegate(IDelegate<void, P> const& original)
		:delegat(original.copy())
	{
	}

	template<class C>
	Delegate(DelegateMethod<C, void, P> const& original)
		:delegat(new DelegateMethod<C, void, P>(original))
	{
	}

	Delegate(DelegateFunction<void, P> const& original)
		:delegat(new DelegateFunction<void, P>(original))
	{
	}

	Delegate(Delegate const& original)
		:delegat(NULL)
	{
		if (original.delegat)
			delegat = original.delegat->copy();
	}
	virtual ~Delegate()
	{
		destroy();
	}


public:
	Delegate& operator=(Delegate const& original)
	{
		destroy();

		if (original.delegat)
			delegat = original.delegat->copy();

		return *this;
	}

	template<class C>
	Delegate& operator=(DelegateMethod<C, void, P> const& original)
	{
		destroy();

		delegat = new DelegateMethod<C, void, P>(original);

		return *this;
	}

	Delegate& operator=(DelegateFunction<void, P> const& original)
	{
		destroy();

		delegat = new DelegateFunction<void, P>(original);

		return *this;
	}

	void operator()(P parameter) const
	{
		if (delegat)
			delegat->operator()(parameter);
		else
			throw Exception("Delegate does not have value");
	}
	bool operator==(IDelegate<void, P> const& other) const
	{
		return this->delegat->operator==(other);
	}
	bool operator!=(IDelegate<void, P> const& other) const
	{
		return this->delegat->operator!=(other);
	}

private:
	void destroy()
	{
		if (delegat)
		{
			delete delegat;
			delegat = NULL;
		}
	}

public:
	IDelegate<void, P>* copy() const
	{
		return new Delegate(*this);
	}


	bool isNull() const
	{
		return this->delegat == NULL || this->delegat->isNull();
	}
};

template<typename R>
class Delegate<R, void> : public IDelegate<R, void>
{
private:
	IDelegate<R, void>* delegat;

public:
	Delegate()
		:delegat(NULL)
	{
	}

	Delegate(IDelegate<R, void> const& original)
		:delegat(original.copy())
	{
	}

	template<class C>
	Delegate(DelegateMethod<C, R, void> const& original)
		:delegat(new DelegateMethod<C, R, void>(original))
	{
	}

	Delegate(DelegateFunction<R, void> const& original)
		:delegat(new DelegateFunction<R, void>(original))
	{
	}

	Delegate(Delegate const& original)
		:delegat(NULL)
	{
		if (original.delegat)
			delegat = original.delegat->copy();
	}
	virtual ~Delegate()
	{
		destroy();
	}


public:
	Delegate& operator=(Delegate const& original)
	{
		destroy();

		if (original.delegat)
			delegat = original.delegat->copy();

		return *this;
	}

	template<class C>
	Delegate& operator=(DelegateMethod<C, R, void> const& original)
	{
		destroy();

		delegat = new DelegateMethod<C, R, void>(original);

		return *this;
	}

	Delegate& operator=(DelegateFunction<R, void> const& original)
	{
		destroy();

		delegat = new DelegateFunction<R, void>(original);

		return *this;
	}

	R operator()() const
	{
		if (delegat)
			return delegat->operator()();
		else
			throw Exception("Delegate does not have value");
	}
	bool operator==(IDelegate<R, void> const& other) const
	{
		return this->delegat->operator==(other);
	}
	bool operator!=(IDelegate<R, void> const& other) const
	{
		return this->delegat->operator!=(other);
	}

private:
	void destroy()
	{
		if (delegat)
		{
			delete delegat;
			delegat = NULL;
		}
	}

public:
	IDelegate<R, void>* copy() const
	{
		return new Delegate(*this);
	}


	bool isNull() const
	{
		return this->delegat == NULL || this->delegat->isNull();
	}
};

template<>
class Delegate<void, void> : public IDelegate<void, void>
{
private:
	IDelegate<void, void>* delegat;

public:
	Delegate()
		:delegat(NULL)
	{
	}

	Delegate(IDelegate<void, void> const& original)
		:delegat(original.copy())
	{
	}

	template<class C>
	Delegate(DelegateMethod<C, void, void> const& original)
		:delegat(new DelegateMethod<C, void, void>(original))
	{
	}

	Delegate(DelegateFunction<void, void> const& original)
		:delegat(new DelegateFunction<void, void>(original))
	{
	}

	Delegate(Delegate const& original)
		:delegat(NULL)
	{
		if (original.delegat)
			delegat = original.delegat->copy();
	}
	virtual ~Delegate()
	{
		destroy();
	}


public:
	Delegate& operator=(Delegate const& original)
	{
		destroy();

		if (original.delegat)
			delegat = original.delegat->copy();

		return *this;
	}

	template<class C>
	Delegate& operator=(DelegateMethod<C, void, void> const& original)
	{
		destroy();

		delegat = new DelegateMethod<C, void, void>(original);

		return *this;
	}

	Delegate& operator=(DelegateFunction<void, void> const& original)
	{
		destroy();

		delegat = new DelegateFunction<void, void>(original);

		return *this;
	}

	void operator()() const
	{
		if (delegat)
			delegat->operator()();
		else
			throw Exception("Delegate does not have value");
	}
	bool operator==(IDelegate<void, void> const& other) const
	{
		return this->delegat->operator==(other);
	}
	bool operator!=(IDelegate<void, void> const& other) const
	{
		return this->delegat->operator!=(other);
	}

private:
	void destroy()
	{
		if (delegat)
		{
			delete delegat;
			delegat = NULL;
		}
	}

public:
	IDelegate<void, void>* copy() const
	{
		return new Delegate(*this);
	}


	bool isNull() const
	{
		return this->delegat == NULL || this->delegat->isNull();
	}
};

#endif