#include <Networking/IPAddress.h>

#include <StringHelpers.h>

using namespace std;

using namespace Networking;

IPAddress::IPAddress(AddressTypes::Value type)
	:type(type)
{
}
AddressTypes::Value IPAddress::getType() const
{
	return this->type;
}

IPAddress const& IPAddress::Any(AddressTypes::Value type)
{
	static IPv4Address const ipv4(0, 0, 0, 0);

	switch (type)
	{
	case AddressTypes::IPv4:
		return ipv4;
		break;
	default:
		throw Exception("Unknown address type");
		break;
	}
}
IPAddress const& IPAddress::Localhost(AddressTypes::Value type)
{
	static IPv4Address const ipv4(127, 0, 0, 1);

	switch (type)
	{
	case AddressTypes::IPv4:
		return ipv4;
		break;
	default:
		throw Exception("Unknown address type");
		break;
	}
}


IPv4Address::IPv4Address(string const& address)
	:IPAddress(AddressTypes::IPv4)
{
	vector<string> parts(StringHelpers::Split(address, ".", false));
	if (parts.size() != 4)
		throw Exception("Invalid format for IPv4 Address");

	for (vector<string>::size_type i = 0; i < 4; i++)
	{
		int nbr = StringHelpers::Parse<int>(parts[i]);

		if (nbr >= 0 && nbr <= 256)
			this->address[i] = nbr;
		else
			throw Exception("Segment " + parts[i] + " of IPv4Address out of bounds");
	}
}
IPv4Address::IPv4Address(uint8_t a, uint8_t b, uint8_t c, uint8_t d)
	:IPAddress(AddressTypes::IPv4)
{
	this->address[0] = a;
	this->address[1] = b;
	this->address[2] = c;
	this->address[3] = d;
}

bool IPv4Address::operator==(IPAddress const& o) const
{
	if (this->getType() == o.getType())
	{
		IPv4Address const& ipv4 = static_cast<IPv4Address const&>(o);
		return ipv4.asBigEndian() == this->asBigEndian();
	}

	return false;
}
bool IPv4Address::operator!=(IPAddress const& o) const
{
	return !(this->operator==(o));
}

uint32_t IPv4Address::asBigEndian() const
{
	return *reinterpret_cast<uint32_t const*>(this->address);
}
uint8_t const* IPv4Address::asBytes() const
{
	return this->address;
}

IPv4Address* IPv4Address::copy() const
{
	return new IPv4Address(*this);
}
string IPv4Address::toString() const
{
	string rv;
	for (int i = 0; i < 4; i++)
	{
		if (i > 0)
			rv += ".";

		rv += StringHelpers::ToString<int>(static_cast<int>(this->address[i]));
	}
	return rv;
}

IPv4Address IPv4Address::FromBigEndian(uint32_t bigEndian)
{
	uint8_t* chr = reinterpret_cast<uint8_t*>(&bigEndian);

	return IPv4Address(chr[0], chr[1], chr[2], chr[3]);
}