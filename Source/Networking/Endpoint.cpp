#include <Networking/Endpoint.h>

using namespace std;
using namespace Networking;

Endpoint::Endpoint()
	:address(NULL),
	port(Endpoint::AnyPort)
{
}
Endpoint::Endpoint(IPAddress const& address, unsigned short port)
	:address(address.copy()),
	port(port)
{
}
Endpoint::Endpoint(Endpoint const& ep)
	:address(ep.isNull() ? NULL : ep.getAddress().copy()),
	port(ep.port)
{
}
Endpoint& Endpoint::operator=(Endpoint const& ep)
{
	this->port = ep.port;

	delete this->address;
	this->address = ep.isNull() ? NULL : ep.getAddress().copy();

	return *this;
}

Endpoint::~Endpoint()
{
	delete this->address;
}

bool Endpoint::isNull() const
{
	return this->address == NULL;
}
IPAddress const& Endpoint::getAddress() const
{
	if (!this->isNull())
		return *this->address;
	else
		throw Exception("Endpoint instance is null");
}
unsigned short Endpoint::getPort() const
{
	if (!this->isNull())
		return this->port;
	else
		throw Exception("Endpoint instance is null");
}

const unsigned short Endpoint::AnyPort(0);