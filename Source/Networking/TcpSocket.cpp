#include <Networking/TcpSocket.h>

#include <Networking/OSSpecific/SocketFunctions.h>
#include <limits>

using namespace std;
using namespace Networking;

struct SocketStatus
{
	enum Value
	{
		Closed			=	0x00,
		Connected		=	0x01,
		Disconnected	=	0x02,
		Bound			=	0x10,
		Listening		=	0x20
	};
};

TcpSocket::TcpSocket()
	:status(SocketStatus::Closed),
	ownsHandle(true)
{
}

TcpSocket::~TcpSocket()
{
	this->close();
}

void TcpSocket::resetHandle(bool ignoreOwnership)
{
	if (this->handle != 0)
	{
		if (this->ownsHandle || ignoreOwnership)
		{
			try
			{
				if (this->status == SocketStatus::Connected)
				{
					try
					{
						socketShutdown(this->handle);
					}
					catch (...)
					{
						socketClose(this->handle);
						throw;
					}
				}

				socketClose(this->handle);
			}
			catch (...)
			{
				this->handle = 0;
				throw;
			}
		}

		this->handle = 0;
	}
}

void TcpSocket::privateClose()
{
	this->resetHandle(false);

	this->status = SocketStatus::Closed;

	this->localEndpoint = Endpoint();
	this->remoteEndpoint = Endpoint();
}

void TcpSocket::assertConnected() const
{
	if (this->status != SocketStatus::Connected)
		throw Exception("This socket is not connected");
}

void TcpSocket::close()
{
	THREADING_SYNCHRONIZE(this->lock);

	this->privateClose();
}

void TcpSocket::connect(Endpoint const& ep)
{
	THREADING_SYNCHRONIZE(this->lock);

	if (this->status != SocketStatus::Closed && this->status != SocketStatus::Disconnected)
		throw Exception("Socket must be closed or disconnected to connect.");

	SocketHandle h = socketCreate();
	try
	{
		Endpoint lEndpoint;
		switch (ep.getAddress().getType())
		{
		case AddressTypes::IPv4:
			socketConnect(h, ep);
			lEndpoint = determineLocalEndpoint(h);
			break;
		default:
			throw Exception("Unknown AddressType");
			break;
		}

		this->handle = h;
		this->remoteEndpoint = ep;
		this->localEndpoint = lEndpoint;
		this->status = SocketStatus::Connected;
	}
	catch (...)
	{
		socketClose(h);
		throw;
	}
}
void TcpSocket::connect(IPAddress const& ip, unsigned short port)
{
	this->connect(Endpoint(ip, port));
}

void TcpSocket::disconnect()
{
	THREADING_SYNCHRONIZE(this->lock);

	if (this->status == SocketStatus::Bound)
		throw Exception("This socket is bound. It may be closed but not disconnected");
	if (this->status == SocketStatus::Listening)
		throw Exception("This socket is listening. It may be closed but not disconnected");
	if (this->status == SocketStatus::Closed)
		throw Exception("This socket is closed.");

	if (this->status == SocketStatus::Connected)
		this->resetHandle(true);
	this->status = SocketStatus::Disconnected;
}

void TcpSocket::bind(Endpoint const& ep)
{
	SocketHandle h = socketCreate();
	try
	{
		this->bindWithHandle(ep, h);
	}
	catch (...)
	{
		socketClose(h);
		throw;
	}
}
void TcpSocket::bind(unsigned short port, IPAddress const& ip)
{
	this->bind(Endpoint(ip, port));
}
void TcpSocket::bindWithHandle(Endpoint const& ep, SocketHandle h)
{
	THREADING_SYNCHRONIZE(this->lock);

	if (this->status != SocketStatus::Closed)
		throw Exception("Socket must be closed to be bound");

	switch (ep.getAddress().getType())
	{
	case AddressTypes::IPv4:
		{
			socketBind(h, ep);
			//Der lokale Endpoint kann sich veraendert haben, bspw. wenn als Port Any uebergeben wurde und das Betriebssystem einen freien Port raussucht.
			Endpoint local(determineLocalEndpoint(h));

			this->handle = h;
			this->status = SocketStatus::Bound;
			this->localEndpoint = local;
		}
		break;
	default:
		throw Exception("Unknown AddressType");
	}
}

void TcpSocket::listen(int backlog)
{
	THREADING_SYNCHRONIZE(this->lock);

	if (this->status != SocketStatus::Bound)
		throw Exception("Socket must be bound to listen");

	socketListen(this->handle, backlog);
	this->status = SocketStatus::Listening;
}

void TcpSocket::accept(TcpSocket& buffer)
{
	THREADING_SYNCHRONIZE(this->lock);

	if (this->status != SocketStatus::Listening)
		throw Exception("Socket must be listening to accept");

	if (buffer.status != SocketStatus::Closed)
		throw Exception("Buffer socket must be closed. Use a newly created instance or call close on it first.");

	Endpoint rEndpoint;
	SocketHandle newSocket;
	Endpoint lEndpoint;
	switch (this->localEndpoint.getAddress().getType())
	{
	case AddressTypes::IPv4:
		newSocket = socketAccept(this->handle, &rEndpoint);
		try
		{
			lEndpoint = determineLocalEndpoint(newSocket);
		}
		catch (...)
		{
			socketClose(newSocket);
			throw;
		}
		break;
	default:
		throw Exception("Bound to unknown address type");
	}

	buffer.handle = newSocket;
	buffer.status = SocketStatus::Connected;
	buffer.remoteEndpoint = rEndpoint;
	buffer.localEndpoint = lEndpoint;
}
TcpSocket* TcpSocket::accept()
{
	TcpSocket* rv = new TcpSocket();
	try
	{
		this->accept(*rv);
	}
	catch (...)
	{
		delete rv;
		throw;
	}
	return rv;
}

uint32_t TcpSocket::getReceivableBytes()
{
	THREADING_SYNCHRONIZE(this->lock);

	this->assertConnected();

	try
	{
		return socketReceivableBytes(this->handle);
	}
	catch (...)
	{
		this->privateClose();
		throw;
	}
}
bool TcpSocket::dataAvailable()
{
	return this->getReceivableBytes() > 0;
}

void TcpSocket::send(char const* data, uint32_t length)
{
	SocketHandle handle;
	THREADING_LOCK(this->lock)
	{
		this->assertConnected();
	
		if (length > static_cast<uint32_t>(numeric_limits<int32_t>::max()))
			throw Exception("Buffer sizes bigger than INT32_MAX not implemented");
		
		handle = this->handle;
	}
	THREADING_RELEASE(this->lock);

	uint32_t totalSent = 0;
	int sent;
	while (totalSent < length)
	{
		try
		{
			sent = socketSend(handle, const_cast<char*>(data + totalSent), length - totalSent);
		}
		catch (...)
		{
			this->close();
			throw;
		}

		totalSent += sent;
	}
}

uint32_t TcpSocket::receive(char* buffer, uint32_t bufferSize)
{
	SocketHandle handle;
	THREADING_LOCK(this->lock)
	{
		this->assertConnected();

		if (bufferSize > static_cast<uint32_t>(numeric_limits<int32_t>::max()))
			throw Exception("buffer sizes bigger than INT32_MAX not implemented");

		handle = this->handle;
	}
	THREADING_RELEASE(this->lock);


	int amount;
	try
	{
		amount = socketReceive(handle, buffer, static_cast<int>(bufferSize));
	}
	catch (...)
	{
		this->close();
		throw;
	}
	
	if (amount == 0)
	{
		this->close();
		throw Exception("Remote host closed connection");
	}

	return amount;
}

bool TcpSocket::isClosed() const
{
	return this->status == SocketStatus::Closed;
}
bool TcpSocket::isConnected() const
{
	return this->status == SocketStatus::Connected;
}
bool TcpSocket::isConnected(bool refreshState)
{
	bool rv = const_cast<TcpSocket const*>(this)->isConnected();
	if (rv && refreshState)
	{
		this->refreshState();
		rv = const_cast<TcpSocket const*>(this)->isConnected();
	}

	return rv;
}
bool TcpSocket::isDisconnected() const
{
	return this->status == SocketStatus::Disconnected;
}
bool TcpSocket::isBound() const
{
	return this->status == SocketStatus::Bound || this->status == SocketStatus::Listening;
}
bool TcpSocket::isListening() const
{
	return this->status == SocketStatus::Listening;
}

void TcpSocket::refreshState()
{
	THREADING_SYNCHRONIZE(this->lock);

	if (this->isConnected())
	{
		try
		{
			bool connected = socketCheckConnected(this->handle);
			if (!connected)
			{
				this->resetHandle(false);
				this->status = SocketStatus::Disconnected;
			}
		}
		catch (...)
		{
			this->close();
			throw;
		}
	}
}

Endpoint const& TcpSocket::getLocalEndpoint() const
{
	return this->localEndpoint;
}
Endpoint const& TcpSocket::getRemoteEndpoint() const
{
	return this->remoteEndpoint;
}

void TcpSocket::setOwnsHandle(bool ownsHandle)
{
	this->ownsHandle = ownsHandle;
}
SocketHandle TcpSocket::getHandle()
{
	return this->handle;
}

SocketHandle TcpSocket::CreateHandle()
{
	return socketCreate();
}