#include <Base.h>
#include <StringHelpers.h>


using namespace std;


OSApiException::OSApiException(string const& message, ErrorCode error)
	:Exception(message + " (Error code: " + StringHelpers::ToString(error) + ")"),
	errorCode(error)
{
}

OSApiException::OSApiException(ErrorCode error)
	:Exception(string("Operating system error (Error code: ") + StringHelpers::ToString(error)),
	errorCode(error)
{
}

OSApiException::ErrorCode OSApiException::getErrorCode() const
{
	return this->errorCode;
}