#include <iostream>

#include <Networking/TcpSocket.h>
#include <Threading/Thread.h>
#include <StringHelpers.h>

using namespace std;
using namespace Networking;
using namespace Threading;

TcpSocket connection;
Mutex lock;

void* receiveLoop(void*)
{
	char buffer[512];
	uint32_t received = 0;
	while (connection.isConnected())
	{
		try
		{
			received = connection.receive(buffer, sizeof(buffer) - 1);
		}
		catch (...)
		{
			break;
		}
		buffer[received] = 0;

		THREADING_LOCK(lock)
		{
			cout << "\rEmpfangen: " << buffer << endl;
			cout << "Senden: ";
			cout.flush();
		}
		THREADING_RELEASE(lock);
	}

	return NULL;
}

void sendLoop()
{
	string line;

	cout << "Senden: ";
	getline(cin, line);
	while (line != "\\q")
	{

		try
		{
			connection.send(line.c_str(), line.size());
		}
		catch (...)
		{
			break;
		}

		THREADING_LOCK(lock)
		{
			cout << "Senden: ";
			cout.flush();
		}
		THREADING_RELEASE(lock);

		getline(cin, line);
	}

	cout << "Verbindung geschlossen" << endl;
	connection.close();
}

void startup()
{
	cout << "Verbunden mit " << connection.getRemoteEndpoint().getAddress().toString() << endl;

	DelegateFunction<void*, void*> threadStart(receiveLoop);
	Thread t(threadStart);
	t.start();

	sendLoop();

	t.waitForTermination();
}

int printHelp()
{
	cout << "Aufruf:\n\tchat <port>\t\tStartet einen Server auf dem gegebenen Port\n\tchat <ip> <port>\tVerbindet sich mit dem Server <ip>:<port>\n";
	return 0;
}
int server(uint16_t port)
{
	TcpSocket server;
	server.bind(port);
	server.listen(1);

	//Den Port aus dem lokalen Endpunkt der Socket auslesen. Falls Port 0 angegeben wurde sucht das Betriebssystem automatisch einen freien Port aus.
	cout << "Warte auf eingehende Verbindung an Port " << server.getLocalEndpoint().getPort() << endl;

	server.accept(connection);
	server.close();

	startup();

	return 0;
}
int client(string ip, uint16_t port)
{
	cout << "Verbinde mit " << ip << ":" << port << endl;
	connection.connect(IPv4Address(ip), port);

	startup();

	return 0;
}

int main(int argc, char** args)
{
	int rv = 0;
	try
	{
		switch (argc)
		{
		case 1:
			{
				rv = printHelp();
				break;
			}
		case 2:
			{
				uint16_t port = StringHelpers::Parse<uint16_t>(args[1]);

				rv = server(port);
				break;
			}
		case 3:
			{
				string ip = args[1];
				uint16_t port = StringHelpers::Parse<uint16_t>(args[2]);

				rv = client(ip, port);
				break;
			}
		default:
			{
				printHelp();
				throw Exception("Unerwartete Parameteranzahl.");
				break;
			}
		}
	}
	catch (Exception& ex)
	{
		cerr << "Error: " << ex.what() << endl;
		rv = 1;
	}

	return rv;
}