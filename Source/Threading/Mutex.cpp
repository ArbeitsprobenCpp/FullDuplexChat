#include <Threading/Mutex.h>

#include <Threading/OSSpecific/ThreadingFunctions.h>

namespace Threading
{
    Mutex::Mutex()
		:initializationError(false)
    {
		try
		{
			mutexCreate(&this->handle);
		}
		catch (OSApiException& ex)
		{
			this->initializationError = true;
			this->initializationErrorCode = ex.getErrorCode();
		}
	}
    Mutex::~Mutex()
    {
		if (!this->initializationError)
			mutexFreeHandle(this->handle);
    }

    void Mutex::lock()
    {
		if (this->initializationError)
			throw OSApiException("Error initializing mutex", this->initializationErrorCode);

		mutexLock(this->handle, INFINITE);
    }

    void Mutex::release()
    {
		if (this->initializationError)
			throw OSApiException("Error initializing mutex", this->initializationErrorCode);
		
		mutexRelease(this->handle);
    }
}