#include <Threading/SynchronizationContext.h>
#include <Threading/Mutex.h>

namespace Threading
{
	//Implementierung des Kontextes mit einem Mutex als Synchronisierungsmechanismus
	template<>
	SynchronizationContext<Mutex>::SynchronizationContext(Mutex& lock)
		:synchronizer(lock)
	{
		this->synchronizer.lock();
	}
	template<>
	SynchronizationContext<Mutex>::~SynchronizationContext()
	{
		this->synchronizer.release();
	}
}