#include <Threading/Thread.h>

#include <Threading/OSSpecific/ThreadingFunctions.h>

#include <Threading/Mutex.h>



namespace Threading
{
	Thread::Thread(ThreadStartRoutine function)
		:running(false),
		freed(true),
		function(function)
	{
	}
	Thread::~Thread()
	{
		if (this->isRunning())
			this->stop();
		if (!this->freed)
			this->freeHandle();
	}

	void Thread::assertRunning() const
	{
		if (!this->running)
			throw Exception("Thread is not running");
	}
	void Thread::assertNotRunning() const
	{
		if (this->running)
			throw Exception("Thread is already running");
	}

	void Thread::freeHandle()
	{
		this->waitForTermination();
		threadFreeHandle(this->threadHandle);
		this->freed = true;
	}

	void Thread::addThreadEndHandler(ThreadEndHandler const& handler)
	{
		THREADING_SYNCHRONIZE(this->handlerLock);

		this->endHandlers.push_back(handler);
	}
	void Thread::removeThreadEndHandler(ThreadEndHandler const& handler)
	{
		THREADING_SYNCHRONIZE(this->handlerLock);

		for (ThreadEndHandlers::iterator it = this->endHandlers.begin(); it != this->endHandlers.end(); it++)
		{
			if ((*it) == handler)
			{
				this->endHandlers.erase(it);
				return;
			}
		}
	}

	void Thread::stop()
	{
		this->assertRunning();
		threadTerminate(this->threadHandle);
	}

	void Thread::start(void* args)
	{
		this->assertNotRunning();

		if (!this->freed)
			this->freeHandle();

		ThreadStartArgs* tsa = new ThreadStartArgs;
		tsa->ActualArgs = args;
		tsa->Threadobject = this;
		
		this->threadHandle = threadStart(Thread::threadStartFunction, tsa);
		this->running = true;
			
		this->freed = !this->running;
		if (!this->running)
			throw Exception("Could not create thread");
	}

	bool Thread::isRunning() const
	{
		return this->running;
	}


	void Thread::waitForTermination(DWORD timeoutMS) const
	{
		if (this->isRunning())
			threadWaitForTermination(this->threadHandle, timeoutMS);
	}

	Mutex Thread::threadlistLock;
	Thread::Threadlist Thread::threads;
	void* Thread::threadStartFunction(void* args)
	{
		ThreadStartArgs* tsa = static_cast<ThreadStartArgs*>(args);

		ThreadId myId = Thread::CurrentId();
		tsa->Threadobject->threadId = myId;

		THREADING_LOCK(Thread::threadlistLock)
		{
			Thread::threads[myId] = tsa->Threadobject;
		}
		THREADING_RELEASE(Thread::threadlistLock);

		void* rv = tsa->Threadobject->function(tsa->ActualArgs);

		THREADING_LOCK(Thread::threadlistLock)
		{
			Thread::threads.erase(myId);
		}
		THREADING_RELEASE(Thread::threadlistLock);

		try
		{
			THREADING_SYNCHRONIZE(tsa->Threadobject->handlerLock);
			for (ThreadEndHandlers::iterator it = tsa->Threadobject->endHandlers.begin(); it != tsa->Threadobject->endHandlers.end(); it++)
				it->operator()(tsa->Threadobject);
		}
		catch (...)
		{
		}
		tsa->Threadobject->running = false;

		return rv;
	}

	const ThreadId Thread::NoThread(0);

	void Thread::Sleep(uint32_t ms)
	{
		threadSleep(ms);
	}
	ThreadId Thread::CurrentId()
	{
		return threadGetCurrentId();
	}
	Thread* Thread::Current()
	{
		Thread* rv = NULL;

		THREADING_LOCK(Thread::threadlistLock)
		{
			Threadlist::iterator it = Thread::threads.find(Thread::CurrentId());
			if (it != Thread::threads.end())
				rv = it->second;
		}
		THREADING_RELEASE(Thread::threadlistLock);

		return rv;
	}
}