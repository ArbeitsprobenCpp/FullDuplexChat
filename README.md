Hier wurde ein minimaler, konsolenbasierter Full-Duplex Chat in C++ realisiert. Das Programm hat einen Servermodus, in dem es an einem Port auf eine eingehende Verbindung wartet, und einen Clientmodus, in dem versucht wird, eine Verbindung zu einer Kombination von IP-Adresse und Port aufzubauen.

Alle Quellcodedateien liegen in den Ordnern [Head](Head) und [Source](Source). Alle Dateien au�er [Main.cpp](Source/Main.cpp) entstammen einer von mir als Lern�bung entwickelten Klassenbibliothek. Es wurden hier nur die f�r den Chat ben�tigten Klassen beigef�gt und so angepasst, dass sie ausserhalb der restlichen Bibliothek lauff�hig sind. Weitere Features der vollst�ndigen Bibliothek sind unter anderem:

* Grundlegende Implementierung des HTTP-Protokolls und darauf aufsetzend eine vollst�ndige Implementierung des WebSocket-Protokolls
* Gleichzeitiges Abh�ren mehrerer Sockets mit Hilfe von `select(...)`
* Verschl�sselung von TCP-Verbindungen mit OpenSSL
* Aufl�sen von Hostnamen �ber DNS
* ThreadPool, der Aufgaben automatisch auf freie Arbeiterthreads aufteilt
* Verwaltung des Dateisystems
* Serialisierung und Deserialisierung in Json
* Erstellung eines Dienstes unter Windows bzw. eines Daemons unter Linux
* Umgang mit Zeitpunkten, Zeitzonen und Zeitspannen

Bei der Gestaltung der Interfaces und Code-Conventions habe ich mich am .NET-Framework orientiert, da ich an diese Umgebung gew�hnt war. Alle Funktionen der Bibliothek wurden so realisiert, dass sie �ber die selben Schnittstellen sowohl unter Windows als auch unter Linux einsetzbar sind.


# Kompilieren

Es folgen Anweisungen zum Kompilieren in unterschiedlichen Umgebungen. Getestet wurde dies sowohl unter *Raspbian GNU/Linux 7 (wheezy)* auf einem Rasbperry PI, als auch unter *Windows 7* mit *Visual Studio 2012 Version 11.0.60610.01 Update 3*.

## Mit cmake

1. In das Verzeichnis mit der CMakeLists.txt navigieren
2. cmake im aktuellen Verzeichnis ausf�hren (Kommando `cmake .`)

Die betriebssystemabh�ngigen Dateien wurden im aktuellen Verzeichnis generiert. Unter Windows wird ein Visual Studio Projekt generiert, w�hrend unter Linux Makefiles generiert werden, die mit `make` ausgef�hrt werden k�nnen.

Getestet wurde dies mit cmake 2.8.10 unter Linux und cmake 3.2.2 unter Windows.

## Ohne cmake

* Das Verzeichnis "Head" muss als Include-Pfad angegeben sein
* Alle .cpp-Dateien im Ordner "Source" und seinen Unterordnern m�ssen kompiliert und gelinkt werden werden

### Linux

* Der Linker ben�tigt die Bibliotheken pthread und rt

### Windows

* _WIN32 muss im Compiler definiert sein
* Der Linker ben�tigt die Bibliothek ws2_32.lib

#### Manuelles Einrichten eines Projektes mit Visual Studio

1. Leeres Konsolenprojekt f�r Visual C++ erstellen
2. Die Ordner "Head" und "Source" in das Projekt aufnehmen
3. In den Projekteigenschaften folgende Einstellungen vornehmen:
	* VC++ Directories: unter Include Directories "$(ProjectDir)Head" hinzuf�gen
	* Linker - Input: unter Additional Dependencies "ws2_32.lib" hinzuf�gen

	
# Nutzung des Programms

* Starten des Programms ohne Parameter gibt einen Hilfetext aus
* Bei Angabe eines Parameters wird dieser als Port interpretiert und die Anwendung wird im Servermodus gestartet: `chat <port>`
* Bei Angabe von zwei Parametern werden diese als IP und Port interpretiert und es wird versucht, eine Verbindung herzustellen: `chat <ip> <port>`
* Wenn erfolgreich eine Verbindung zwischen zwei Instanzen hergestellt wurde k�nnen Nachrichten zur �bertragung in der Konsole eingegeben und mit Enter best�tigt werden
* Um das Programm nach Herstellen einer Verbindung ordnungsgem�� zu beenden wird \q eingegeben und mit Enter best�tigt

Hat der Kommunikationspartner die Verbindung beendet, so wird dies nicht direkt in der lokalen Instanz angezeigt. Der Grund liegt in der Implementierung des Sendens �ber blockierendes Lesen von std::cin, das ohne Benutzereingabe nicht trivial unterbrochen werden kann. Die lokale Instanz zeigt den Verbindungsabbruch an und beendet sich, sobald die n�chste Konsoleneingabe mit Enter best�tigt wird.